### Config
SHELL:=/bin/bash
.ONESHELL:
.SHELLFLAGS: -euo pipefail -O extglob
.DEFAULT_GOAL:=help
.PHONY: help all default mk-config ${APP_TARGETS}
MAKEFLAGS += --no-builtin-rules

MKUP_PATH := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))

INCLUDE_ENV_FILES ?= dot.env .env .env.local # Config in environment files, may be not exist
-include ${INCLUDE_ENV_FILES}

### Params
APP_ENV   ?= dev
APP_DEBUG ?= true
# Paths
BIN       ?= bin
CONFIG    ?= config
SOURCE    ?= src
BUILD     ?= build
REPORT    ?= ${BUILD}/reports

${BIN} ${CONFIG} ${SOURCE} ${BUILD} ${REPORT}:
	mkdir -p ${@}
##
DOWNLOAD := curl -SL --progress-bar -o

comma := ,
empty :=
space := $(empty) $(empty)

####
CLR_GREY:=\033[30;1m
CLR_RED:=\033[31;1m
CLR_GREEN:=\033[32;1m
CLR_YELLOW:=\033[33;1m
CLR_BLUE:=\033[34;1m
CLR_CYAN:=\033[36;1m
CLR_WHITE:=\033[39;1m
CLR_OFF:=\033[0m

# Verbose (1, 0)
V = 0
# If verbose don't put @
Q = $(if $(filter 1,$V),,@)
# Prompt for messages
M = $(shell printf "${CLR_BLUE}▶${CLR_OFF}")
# Info
I = $Q echo -e "\n${M}"

### Targets
help:                                   ## Show this help
	$Q
	echo
	TITLE_CURRENT=""
	for MK_FILE in Makefile $(sort $(filter-out Makefile, ${MAKEFILE_LIST})); do
		TARGET=$$( awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / { printf "%s:%s#n", $$1, $$2; }' $${MK_FILE} )
		if [[ "$${TARGET}" != "" ]]; then
			TITLE=$$(basename $${MK_FILE%.*})  # Title is name of file without extension
			TITLE=$${TITLE%%-*}                # and the beginning part until the first dash (-)
			if [[ "$${TITLE}" == "Makefile" ]]; then
				TITLE=app
			fi
			if [[ "$${TITLE_CURRENT}" != "$${TITLE}" ]]; then
				echo -e "${CLR_GREEN}☛${CLR_WHITE}" $${TITLE//_/ }… "${CLR_OFF}"      # Replace '_' by spaces
				TITLE_CURRENT=$${TITLE}
			fi
			echo -n $${TARGET} | sed "s/#n/\\n/g" | awk 'BEGIN {FS = ":"} { printf "    ${CLR_CYAN}%-18s${CLR_OFF} %s\n", $$1, $$2; }'
		fi
	done
	echo

help-list:
	$I available rules…
	awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "    ${CLR_CYAN}%-18s${CLR_OFF} %s\n", $$1, $$2}' $(MAKEFILE_LIST)
	echo

help-config:                                   ## Show config variables
	$Q
	echo
	TITLE_CURRENT=""
	for MK_FILE in $(sort ${MAKEFILE_LIST}); do
		TARGET=$$( awk 'BEGIN {FS = "?="}/^[a-zA-Z_-]+[ ]*\?=.*/ { printf "%s#%s#n#", $$1, $$2; }' $${MK_FILE} )
		if [[ "$${TARGET}" != "" ]]; then
			TITLE=$$(basename $${MK_FILE%.*})  # Title is name of file without extension
			TITLE=$${TITLE%%-*}                # and the beginning part until the first dash (-)
			if [[ "$${TITLE}" == "Makefile" ]]; then
				TITLE=main
			fi
			if [[ "$${TITLE_CURRENT}" != "$${TITLE}" ]]; then
				echo -e "${CLR_GREEN}☛${CLR_WHITE}" $${TITLE//_/ }… "${CLR_WHITE}"      # Replace '_' by spaces
				TITLE_CURRENT=$${TITLE}
			fi
			echo -n $${TARGET} | sed "s/#n#/\\n/g" | awk 'BEGIN {FS = "#"} { printf "    ${CLR_CYAN}%-22s${CLR_OFF}=${CLR_YELLOW}%s${CLR_GREY}%s%s${CLR_OFF}\n", $$1, $$2, ($$3!="")?"    #":"", $$3; }' | sort
		fi
	done
	echo
