## Choose one...
# : ${PHP_CLI:="php"}
: ${PHP_CLI:="php-docker"}
# : ${PHP_CLI:="php-docker-compose"}
## Choose one...
: ${PHP_DEBUG:="php-xdebug"}
# : ${PHP_DEBUG:="php-xdebug2"}
: ${COMPOSER_HOME:=/home/composer}
##
DOCKER_RUN_OPTIONS+="
  --env PHP_CLI=php
"
DOCKER_BUILD_OPTIONS+="
  --build-arg COMPOSER_HOME=${COMPOSER_HOME}
"
##
php-cli() {
  ${PHP_CLI} "$@"
}

php-docker() {
  docker-run ${DOCKER_RUN_EXTRA} ${DOCKER_IMAGE_RUN} php "$@"
}

php-docker-compose() {
  docker-compose-run ${DOCKER_SERVICE_RUN} php "$@"
}

php-dbg() {
  ${PHP_DEBUG} "$@"
}

php-xdebug() {
  ${PHP_CLI} \
    -dzend_extension=xdebug.so \
    -dxdebug.mode=develop,debug,coverage \
    -dxdebug.start_with_request=yes \
    -dxdebug.client_host=${XDEBUG_REMOTE_PORT:-172.17.0.1} \
    -dxdebug.client_port=${XDEBUG_REMOTE_HOST:-9003} \
    "$@"
}

php-xdebug2() {
  ${PHP_CLI} \
    -dzend_extension=xdebug.so \
    -dxdebug.coverage_enable=1 \
    -dxdebug.remote_enable=1 \
    -dxdebug.remote_autostart=1 \
    -dxdebug.remote_port=${XDEBUG_REMOTE_PORT:-172.17.0.1} \
    -dxdebug.remote_host=${XDEBUG_REMOTE_HOST:-9000} \
    "$@"
}

composer-init() {
  local NAME=${1:-${APP_PROJECT}/${APP_NAME}}
  php-cli ${TOOLS_BIN}/composer init --no-interaction \
    --type project \
    --license WTFPL \
    --stability stable \
    --name "$NAME"
}

composer-download() {
  [ -e "${TOOLS_BIN}/composer" ] && return 0
  echo installing composer…
  mkdir -p "${TOOLS_BIN}"
  aliases-download "${TOOLS_BIN}"/composer-installer https://getcomposer.org/installer || return $?
  aliases-exec ${PHP_CLI} -f "${TOOLS_BIN}"/composer-installer -- \
    --install-dir="${TOOLS_BIN}" \
    --filename=composer \
    --version=${PHP_COMPOSER_VERSION:-2.5.5} || return $?
  rm -rf "${TOOLS_BIN}"/composer-installer
}

composer() {
  DOCKER_RUN_EXTRA="
    --env COMPOSER_HOME=${COMPOSER_HOME}
    --volume composer:${COMPOSER_HOME}
  "
  php-cli ${TOOLS_BIN}/composer "$@"
}

alias phpunit="php-cli ${VENDOR_BIN}/phpunit"
alias phpunit-coverage="php-dbg ${VENDOR_BIN}/phpunit --coverage-text"
alias kahlan="php-dbg ${VENDOR_BIN}/kahlan --reporter=verbose"
alias behat="php-cli ${VENDOR_BIN}/behat"
alias phpstan="php-cli ${VENDOR_BIN}/phpstan"
alias console="php-cli ${TOOLS_BIN}/console"

##
aliases-var PHP_CLI PHP_DEBUG COMPOSER_HOME
aliases-fnc composer composer-download composer-init php-cli php-docker php-docker-compose php-dbg php-xdebug php-xdebug2
aliases-alias phpunit phpunit-coverage kahlan behat phpstan console
