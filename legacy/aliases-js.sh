## Choose one...
# : ${NODE_CLI:="node"}
: ${NODE_CLI:="${TOOLS_BIN}/node"}

##
node-cli() {
  aliases-exec ${NODE_CLI} "$@"
}

npm() {
  PATH=${TOOLS_BIN}:${PATH} aliases-exec ${TOOLS_BIN}/npm "$@"
}

npx() {
  PATH=${TOOLS_BIN}:${PATH} aliases-exec ${TOOLS_BIN}/npx "$@"
}

##
aliases-var NODE_CLI
aliases-fnc node-cli npm npx
