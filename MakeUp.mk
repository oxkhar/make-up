##
MKUP_PATH := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))
MKUP_INCLUDE := ${MKUP_PATH}/mk
MKUP_TYPE := project

## Default components
MKUP_HAS ?= docker-engine git-trunk php-composer php-tests-xunit php-qa

## Main initialization...
include ${MKUP_PATH}/core.mk
# Init project
include ${MKUP_PATH}/${MKUP_TYPE}.mk   # This has to be the first include

## Docker
MKUP_DOCKER := docker-engine docker-compose
MKUP_DOCKER := $(lastword $(filter ${MKUP_DOCKER},${MKUP_HAS}))
ifneq (${MKUP_DOCKER},)
	include ${MKUP_INCLUDE}/${MKUP_DOCKER}.mk
endif

## Tag version
MKUP_BRANCH := git-trunk git-flow
MKUP_BRANCH := $(lastword $(filter ${MKUP_BRANCH},${MKUP_HAS}))
ifneq (${MKUP_BRANCH},)
	TAG_FLOW ?= ${MKUP_BRANCH}
	include ${MKUP_INCLUDE}/tag.mk
endif

## JavaScript
MKUP_JS := js-node
MKUP_JS := $(filter ${MKUP_JS},${MKUP_HAS})
ifneq (${MKUP_JS},)
	include $(addprefix ${MKUP_INCLUDE}/,$(addsuffix .mk, ${MKUP_JS}))
endif

## PHP
MKUP_PHP := php-build php-composer php-doctrine php-symfony php-tests-xunit php-tests-mutant php-tests-rspec php-tests-bdd php-qa
MKUP_PHP := $(filter ${MKUP_PHP},${MKUP_HAS})
ifneq (${MKUP_PHP},)
	include ${MKUP_INCLUDE}/php.mk
	#
	MKUP_PHP := ${MKUP_PHP:php-doctrine%=php_doctrine%}
	MKUP_PHP := ${MKUP_PHP:php-symfony%=php_symfony%}
	MKUP_PHP := ${MKUP_PHP:php-tests%=php_tests%}
	include $(addprefix ${MKUP_INCLUDE}/,$(addsuffix .mk,$(filter-out php-qa,${MKUP_PHP})))
	##
	# Depends of tests for report coverage, to work fine not include before
	ifneq ($(filter php-qa php_qa,${MKUP_PHP}),)
		include ${MKUP_INCLUDE}/php_qa.mk
	endif
endif

##
include ${MKUP_PATH}/install.mk   # This has to be the last include
