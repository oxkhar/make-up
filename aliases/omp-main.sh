
: ${OMP_PS1:=\
"$(text-colorize -p fg_red ╭─)\b \z⫘ \g\p\k \c\n"\
"$(text-colorize -p fg_red ╰─)\u@\h:\w…\$"\
" "}

. "${ALIASES_PATH}/omp-git.sh"

function aliases-update-prompt() {
  local -i return=$?
  PS1="${OMP_PS1//"\b"/${1:-}}"

  aliases-colorize-prompt

  aliases-replace-prompt '\c' "aliases-return-code-prompt $return"
  aliases-replace-prompt '\g' aliases-git-prompt
  aliases-replace-prompt '\p' aliases-python-prompt
  aliases-replace-prompt '\k' aliases-k8s-prompt
  aliases-replace-prompt '\z' aliases-docker-prompt
}

function aliases-colorize-prompt() {
  PS1="${PS1//"\u"/$(text-colorize -p fg_green bold '\u')}"
  PS1="${PS1//"\h"/$(text-colorize -p fg_green bold '\h')}"
  PS1="${PS1//"\u"*@*"\h"/$(text-colorize -p fg_green bold '\u@\h')}"
  PS1="${PS1//"\w"/$(text-colorize -p fg_blue bold '\w')}"
  PS1="${PS1//"\$"/$(text-colorize -p fg_red '\$')}"
}

function aliases-replace-prompt() {
  local code=${1?'param "code" is null'}
  local p=${2?'param "format" is null'}

  [ "${PS1/*${code}*/${code}}" == "${code}" ] &&
    PS1="${PS1//"${code}"/$(${p})}"
}

function aliases-return-code-prompt() {
  local return_code=${1?'param "return_code" is null'}

  text-display_if not_zero $return_code "  \t{value}↵" "fg_red bold"
}

# Determine active Python virtualenv details.
function aliases-python-prompt() {
  [[ -z "${VIRTUAL_ENV:-}" ]] && return 0

  text-display_if not_zero "$(basename "${VIRTUAL_ENV}")" "{value}" "fg_blue"
}

function aliases-k8s-prompt() {
  aliases-k8s-available || return 0
  local context=$(kubectl config current-context 2> /dev/null)

  text-display_if not_empty "$context" "["
  text-display_if not_empty "$context" "☸ {value}" "fg_green bold"
  text-display_if not_empty "$context" "]"
}

function aliases-k8s-available() {
    kubectl version 2> /dev/null 1>&2
}

function aliases-docker-prompt() {
  aliases-docker-available || return 0

  text-colorize -p "["
  text-colorize -p fg_blue bold "🐳…"

  local containers="*$(docker-compose ps --services --status running | grep -v "^$" | tr [[:space:]] '*')*"
  for service in $(docker-compose config --services | sort); do
    local port=$(docker-compose config $service | grep "published" | tail -1 | cut -f2 -d':' | tr -d '"[:space:]')
    local icon=$(bud-icon-for-service $service)

    local color="fg_red bold"
    [ "${containers//*\*${service}\**/}" == "" ] && color="fg_green bold"

    text-colorize -p $color " ${port:-n/a}${icon} "
  done
  text-colorize -p "]"
}

function aliases-docker-available() {
    [ -e "${DOCKER_COMPOSE_FILE}" ]
}

function aliases-icon-for-service() {
  case $1 in
    database*) echo "️🛢";;
    redis*) echo "🔄";;
    elasticsearch*) echo "🔍";;
    kibana*) echo "📊";;
    *) echo "🚀";;
  esac
}
