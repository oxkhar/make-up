
help-title-core() {
  text-colorize bold " ‣ "
  text-colorize fg_red bold "$(text-capitalize ${PROJECT_NAME})"
  echo "…"
}

help-about-core() {
  return
}

help-info-core() {
  case ${1:-} in
    *)
      help-aliases 2>/dev/null || true
      ;;
  esac
}
