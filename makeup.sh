#!/bin/bash

MKUP_ACTION=${1:-install}
MKUP_PATH=${2:-.makeUp}

MKUP_NAME=makeup
MKUP_BRANCH=master
MKUP_FILE=${MKUP_NAME}-${MKUP_BRANCH}
MKUP_URL="https://gitlab.com/oxkhar/${MKUP_NAME}/-/archive/${MKUP_BRANCH}/${MKUP_FILE}.tar.gz"

rm -rf ./${MKUP_PATH}
mkdir -p ${MKUP_PATH}/config bin
curl -sL ${MKUP_URL} | tar xfz - --atime-preserve -C ${MKUP_PATH} || exit $?

MKUP_DOWN=${MKUP_PATH}/${MKUP_FILE}
mv ${MKUP_DOWN}/{*.sh,*.mk,mk,aliases} ${MKUP_PATH}

cp -a ${MKUP_DOWN}/example/config/tools/ ${MKUP_PATH}/config/
cp -a ${MKUP_PATH}/aliases/core.sh ./bin/aliases.sh

if [[ "${MKUP_ACTION}" == "init" ]]; then
  [ -e config ] || mv -u ${MKUP_DOWN}/example/config . && rm -rf ${MKUP_DOWN}/example/config
  mv -u ${MKUP_DOWN}/example/bin/* ./bin/ && rm -rf ${MKUP_DOWN}/example/bin
  mv -u ${MKUP_DOWN}/example/{.env,.gitignore,.dockerignore,.editorconfig,*} .
fi

rm -rf ${MKUP_DOWN}
