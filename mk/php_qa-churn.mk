
## Params
PHP_CHURN_OPTIONS ?= #

## Config
PHP_CHURN_BIN    = ${BIN}/churn
PHP_CHURN        = $(PHP_CLI) ${APP_BIN}/churn run
PHP_CHURN_CONFIG = churn.yml
PHP_CHURN_REPORT = ${REPORT}/php-churn
#
CONF_FILES += ${PHP_CHURN_CONFIG}
UNINSTALL_FILES += ${PHP_CHURN_BIN}

QA_TOOLS_BIN       += ${PHP_CHURN_BIN}
QA_TARGETS_ANALYSE += php-churn
QA_TARGETS_REPORTS += php-churn-report
##
php-churn-report: ${PHP_CHURN_CONFIG} ${REPORT} | ${PHP_CHURN_BIN}
	$I PHP churn reports…
	mkdir -p ${PHP_CHURN_REPORT}

	$(PHP_CHURN) --format json ${PHP_CHURN_OPTIONS} ${SOURCE} > ${PHP_CHURN_REPORT}/report.json
	$(PHP_CHURN) --format csv ${PHP_CHURN_OPTIONS} ${SOURCE} > ${PHP_CHURN_REPORT}/report.csv
	$(PHP_CHURN) --format text ${PHP_CHURN_OPTIONS} ${SOURCE} > ${PHP_CHURN_REPORT}/report.txt

	echo -e "Builded..."

php-churn: ${PHP_CHURN_CONFIG} | ${PHP_CHURN_BIN}                        ## PHP churn
	$I PHP churn…

	$(PHP_CHURN) ${PHP_CHURN_OPTIONS} ${SOURCE}

${PHP_CHURN_BIN}: | composer-plugins
	$I Installing PHP churn…

	$(COMPOSER) bin churn require bmitch/churn-php &&
	touch ${PHP_CHURN_BIN}
