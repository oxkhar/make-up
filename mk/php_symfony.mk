
## Params
SF_CONSOLE_OPTIONS ?=

## Config
SF_CONSOLE_BIN = ${BIN}/console
SF_CONSOLE     = $(PHP_CLI) ${APP_BIN}/console

DOT_ENV_FILES  = .env
APP_ENV_FILE   = .env.local
#
UNINSTALL_FILES += ${DOCTRINE_BIN} ${MIGRATION_BIN}

SF_TARGETS  += sf-install sf-database sf-cache sf-fixtures sf-migrations sf-assets sf-cache-db

APP_TARGETS += ${SF_TARGETS} sf-docker-options

DOCKER_RUN_OPTIONS += ${SF_DOCKER_RUN_OPTIONS}
${SF_TARGETS}: SF_DOCKER_RUN_OPTIONS = --env APP_ENV=${APP_ENV} --env APP_DEBUG=${APP_DEBUG}

install: sf-install

##
sf-install: | sf-cache sf-assets         ## Install Symfony
	$I Install Symfony…

sf-database: | sf-cache                  ## Drop and remove DB schemas
	$I Update database…
	$(SF_CONSOLE) doctrine:database:drop ${SF_CONSOLE_OPTIONS} --force
	$(SF_CONSOLE) doctrine:database:create ${SF_CONSOLE_OPTIONS}
	$(SF_CONSOLE) doctrine:schema:create ${SF_CONSOLE_OPTIONS}

sf-fixtures:                           ## Load fixtures into database
	$I Loading fixtures…
	$(SF_CONSOLE) doctrine:fixtures:load ${SF_CONSOLE_OPTIONS} --no-interaction

sf-migrations:                         ## Run database migrations
	$I Running migrations…
	$(SF_CONSOLE) doctrine:migrations:migrate ${SF_CONSOLE_OPTIONS} -vv --all-or-nothing --no-interaction --allow-no-migration

sf-assets:                           ## Load fixtures into database
	$I Generate assets…
	$(SF_CONSOLE) assets:install ${SF_CONSOLE_OPTIONS} --no-interaction

sf-cache:                              ## Clear cache
	$I Cache clear…
	$(SF_CONSOLE) cache:clear ${SF_CONSOLE_OPTIONS}
	$(SF_CONSOLE) cache:warmup ${SF_CONSOLE_OPTIONS}

sf-cache-db:                           ## Clear database cache
	$I Cache Database clear…
	$(SF_CONSOLE) doctrine:cache:clear-collection-region ${SF_CONSOLE_OPTIONS}
	$(SF_CONSOLE) doctrine:cache:clear-entity-region ${SF_CONSOLE_OPTIONS}
	$(SF_CONSOLE) doctrine:cache:clear-metadata ${SF_CONSOLE_OPTIONS}
	$(SF_CONSOLE) doctrine:cache:clear-query ${SF_CONSOLE_OPTIONS}
	$(SF_CONSOLE) doctrine:cache:clear-query-region ${SF_CONSOLE_OPTIONS}
	$(SF_CONSOLE) doctrine:cache:clear-result ${SF_CONSOLE_OPTIONS}
	$(SF_CONSOLE) cache:warmup ${SF_CONSOLE_OPTIONS}

.env.local.php: env
	$I Creating Symfony configuration…
	$(COMPOSER) dump-env ${APP_ENV}

sf-build: APP_ENV := prod
sf-build: APP_DEBUG := false
sf-build: SF_CONSOLE_OPTIONS += --env=prod --no-debug
sf-build: COMPOSER_OPTIONS += --no-scripts --ignore-platform-reqs --optimize-autoloader --prefer-dist --no-dev
sf-build: vendor clean .env.local.php sf-cache sf-assets ${BUILD}        ## Symfony build project
	$I Creating project…
	mkdir -p ${BUILD}/{var,config}
	cp -a .dockerignore .env.local.php public src translations templates vendor ${BUILD}
	cp -a var/cache ${BUILD}/var/
	cp -a config/{bundles.php,preload.php} ${BUILD}/config
