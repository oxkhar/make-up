
## Params
PHPINSIGHTS_OPTION ?= # --disable-security-check

## Config
PHPINSIGHTS_BIN    = ${VENDOR_BIN}/phpinsights
PHPINSIGHTS        = $(PHP_CLI) ${APP_BIN}/phpinsights
PHPINSIGHTS_ARGS   = analyse --no-interaction

PHPINSIGHTS_CONFIG = phpinsights.php
PHPINSIGHTS_REPORT = ${REPORT}/phpinsights
#
UNINSTALL_FILES += phpinsights.php ${PHPINSIGHTS_BIN}
CONF_FILES += ${PHPINSIGHTS_CONFIG}

QA_TOOLS_BIN       += ${PHPINSIGHTS_BIN}
QA_TARGETS_ANALYSE += phpinsights
QA_TARGETS_REPORTS += phpinsights-report
##
phpinsights-report: ${PHPINSIGHTS_CONFIG} ${REPORT} | ${PHPINSIGHTS_BIN}
	$I PHP Insights reports…
	mkdir -p ${PHPINSIGHTS_REPORT}

	$(PHPINSIGHTS) ${PHPINSIGHTS_ARGS} --format=json ${PHPINSIGHTS_OPTIONS} ${SOURCE} \
		> ${PHPINSIGHTS_REPORT}/report.json 2> /dev/null
	$(PHPINSIGHTS) ${PHPINSIGHTS_ARGS} --format=checkstyle ${PHPINSIGHTS_OPTIONS} ${SOURCE} \
		> ${PHPINSIGHTS_REPORT}/checkstyle.xml 2> /dev/null

	echo -e "Builded..."

phpinsights: ${PHPINSIGHTS_CONFIG} | ${PHPINSIGHTS_BIN}                  ## PHP Insights
	$I PHP Insights…

	$(PHPINSIGHTS) ${PHPINSIGHTS_ARGS} ${PHPINSIGHTS_OPTIONS} ${SOURCE}

${PHPINSIGHTS_BIN}: | composer-plugins
	$I Installing PHP Insights…

	$(COMPOSER) bin phpinsights config allow-plugins.dealerdirect/phpcodesniffer-composer-installer true
	$(COMPOSER) bin phpinsights require nunomaduro/phpinsights &&
	touch ${PHPINSIGHTS_BIN}
