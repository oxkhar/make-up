
## Config
PHPMETRICS_VERSION ?= 2.7.4
PHPMETRICS_JUNIT   ?= # --junit=${REPORT}/junit.xml
PHPMETRICS_OPTIONS ?= # --exclude=var,vendor,bin --quiet

## Params
PHPMETRICS_BIN      = ${VENDOR_BIN}/phpmetrics
PHPMETRICS          = $(PHP_CLI) ${APP_BIN}/phpmetrics
PHPMETRICS_ARGS	    = --git=$(shell which git) \
                      --report-html=${PHPMETRICS_REPORT} \
                      --report-csv=${PHPMETRICS_REPORT}/report.csv \
                      --report-json=${PHPMETRICS_REPORT}/report.json \
                      --report-violations=${PHPMETRICS_REPORT}/violations.xml

PHPMETRICS_REPORT   = ${REPORT}/metrics
#
UNINSTALL_FILES += ${PHPMETRICS_BIN}

QA_TOOLS_BIN       += ${PHPMETRICS_BIN}
QA_TARGETS_ANALYSE +=
QA_TARGETS_REPORTS += phpmetrics phpmetrics-tests
##
# Not valid for RSpec, only for PHPUnit
phpmetrics-tests: PHPMETRICS_JUNIT = --junit=${REPORT}/junit.xml
phpmetrics-tests: tests-report phpmetrics

phpmetrics: | ${PHPMETRICS_BIN}                   ## PHP Metrics
	$I PHP Metrics…

	mkdir -p ${PHPMETRICS_REPORT}

	$(PHPMETRICS) ${PHPMETRICS_JUNIT} ${PHPMETRICS_OPTIONS} ${PHPMETRICS_ARGS} .

${PHPMETRICS_BIN}: | composer-plugins
	$I installing PHP Metrics…

	$(COMPOSER) bin phpmetrics require --dev phpmetrics/phpmetrics &&
	touch ${PHPMETRICS_BIN}
