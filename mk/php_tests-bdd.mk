
## Params
BEHAT_OPTIONS ?= --colors

## Config
BEHAT_BIN = ${VENDOR_BIN}/behat
BEHAT     = $(PHP_CLI) ${APP_BIN}/behat
#
BDD_TARGETS  := bdd-verbose
BDD_TARGETS  += features/* features/*/*.feature features/*/*/*.feature
BDD_ARGS     := -f progress
#
UNINSTALL_FILES += ${BEHAT_BIN}

APP_TARGETS += bdd ${BDD_TARGETS}
##
bdd: | ${BEHAT_BIN}                       ## Run BDD features
	$I running $(BDD_NAME:bdd-%=% ) features…
	$(BEHAT) ${BEHAT_OPTIONS} ${BDD_ARGS} -- ${BDD_FEATURES}

bdd-verbose: BDD_ARGS := -f pretty -v     ## Run BDD features with description

${BDD_TARGETS}: BDD_NAME := $(filter bdd-%, ${MAKECMDGOALS})
${BDD_TARGETS}: BDD_FEATURES ?= $(filter features/%, ${MAKECMDGOALS})
${BDD_TARGETS}: bdd
