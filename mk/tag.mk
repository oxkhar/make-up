
## Params
# Files that contains version app and needs replace with the new tag version
# Contains file with access path
TAG_APP_FILES  ?= # config/settings.php app/main.go
# How merge flow will apply when create a new release...
TAG_FLOW       ?= git-trunk # Values: git-flow | git-trunk

TAG_RELEASE_PREFIX ?= release
TAG_MAIN_BRANCH    ?= master
TAG_DEV_BRANCH     ?= dev

## Config
RELEASE_BRANCH = ${TAG_RELEASE_PREFIX}/${TAG}

CHAG_VERSION  := 1.1.4
CHAG          := ${BIN}/chag

TAG_FLOW_TASK := tag-${TAG_FLOW}
TAG_TARGETS   := tag tag-git-flow tag-git-trunk tag-init tag-release tag-master \
                 tag-tag tag-finish tag-clean tag-changelog tag-version
#
UNINSTALL_FILES	+= ${CHAG}

APP_TARGETS += ${TAG_TARGETS}
##
# if TAG is empty value is searched from release branch with format "release/1.2.3"
${TAG_TARGETS}: TAG ?= $(filter v%,$(subst refs/heads/release/,,$(shell git symbolic-ref -q HEAD 2> /dev/null)))

tag-git-flow:  tag-release tag-master tag-tag tag-finish tag-clean
tag-git-trunk: tag-release tag-master tag-tag tag-clean

tag: ${TAG_FLOW_TASK}               ## Create new release with tag version

tag-push: tag
	$I Push changes into remote repository…
	git checkout ${TAG_MAIN_BRANCH}
	git push origin ${TAG_MAIN_BRANCH} --tags
	if [[ "${TAG_FLOW}" == "git-flow" ]]; then
	 	git checkout ${TAG_DEV_BRANCH}
		git push origin ${TAG_DEV_BRANCH}
	fi

tag-init:
	$I Initialize…
	$(if ${TAG},,$(error TAG is not defined. Pass via "make tag TAG=v4.2.1"))
	$(if ${RELEASE_BRANCH},,$(error Release branch is not defined))
	git fetch --all --prune

tag-master: tag-init | $(CHAG)
	$I Merge into main branch ${TAG_MAIN_BRANCH}…
	git checkout -B ${TAG_MAIN_BRANCH} origin/${TAG_MAIN_BRANCH}
	git merge --no-ff -m "merge ${RELEASE_BRANCH} into ${TAG_MAIN_BRANCH}" ${RELEASE_BRANCH}

tag-tag:
	$I Creating tag ${TAG}…
	$(CHAG) tag --force

tag-finish: tag-init
	$I Merge into develop branch ${TAG_DEV_BRANCH}…
	git checkout -B ${TAG_DEV_BRANCH} origin/${TAG_DEV_BRANCH}
	git merge --no-ff -m "merge ${RELEASE_BRANCH} to ${TAG_DEV_BRANCH}" ${RELEASE_BRANCH}

tag-clean:
	$I Cleaning release branch ${RELEASE_BRANCH}…
	git branch -D ${RELEASE_BRANCH}

tag-release: tag-version
	$I Init release ${TAG}…
	git checkout -B ${RELEASE_BRANCH}
	git commit -m "${TAG} release"

tag-version: tag-init tag-changelog ${TAG_APP_FILES:%=%.tag}
	$I Tag version file…
	echo ${TAG} > VERSION
	git add VERSION

tag-changelog: | $(CHAG)
	$I Tag Changelog…
	$(CHAG) update ${TAG}
	git add CHANGELOG.md

${TAG_APP_FILES:%=%.tag}::
	$I Tag source ${@:%.tag=%}…
	sed -e "s${TAG_REG_EXP}" ${@:%.tag=%} > ${@} && cat ${@} > ${@:%.tag=%} && rm ${@}
	git add ${@:%.tag=%}

# tools...
${CHAG}: | ${BIN}
	$I installing chag…
	$(DOWNLOAD) "${CHAG}" \
		"https://github.com/mtdowling/chag/releases/download/${CHAG_VERSION}/chag" \
		|| exit $$?
		chmod +x ${CHAG}
