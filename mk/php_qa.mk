
## Params
QA_MODULES ?= codesniffer copy_paste_detector cs_fixer insights loc mess_detector metrics phpstan
QA_MODULES_DISABLED += churn  # not activated, is not useful

#  phpmd not activated by default
QA_ANALYSE ?= phpstan php-cs-fixer phpcs phploc phpcpd phpinsights
QA_ANALYSE_DISABLED += php-churn  # not activated

# phpinsights-report   # By default not include because is slow and is not user helper
QA_REPORTS ?= phpstan-report php-cs-fixer-report phpcs-report \
              phpmd-report phpcpd-report phploc-report \
              ${QA_REPORTS_TESTS}
QA_REPORTS_DISABLED += php-churn-report  # not activated, is not useful
# phpmetrics-test  # For PHPUnit tests
# phpmetrics       # For Kahlan tests
QA_REPORTS_TESTS ?= phpmetrics

## Config
QA_TOOLS_BIN  +=
QA_TARGETS_ANALYSE +=
QA_TARGETS_REPORTS +=
-include $(addprefix ${MKUP_PATH}php_qa-, $(addsuffix .mk, $(filter-out ${QA_MODULES_DISABLED},${QA_MODULES})))

QA_ANALYSE_TARGETS = $(filter ${QA_TARGETS_ANALYSE},$(filter-out ${QA_ANALYSE_DISABLED},${QA_ANALYSE}))
QA_REPORTS_TARGETS = $(filter ${QA_TARGETS_REPORTS},$(filter-out ${QA_REPORTS_DISABLED},${QA_REPORTS}))

APP_TARGETS += install-qa-tools analyse reports ${QA_TARGETS_ANALYSE} ${QA_TARGETS_REPORTS}
##
${QA_TARGETS_REPORTS}: DOCKER_TTY_OPTIONS := ${DOCKER_TTY_ATTACH}

install-qa-tools: ${QA_TOOLS_BIN}
	$I QA tools installed…

analyse: ${QA_ANALYSE_TARGETS}        ## Quality code analysis

reports: clean ${QA_REPORTS_TARGETS}  ## Quality code reports
