
## Params
NODE_VERSION ?= 16.13.1
NODE_OPTIONS ?= #

NVM_VERSION  ?= 0.39.1

## Config
NPM_BIN   = ${BIN}/npm
NPM       = $(NODE) ${APP_BIN}/npm
NPX_BIN   = ${BIN}/npx
NPX       = $(NODE) ${APP_BIN}/npx
NODE_BIN  = ${BIN}/node
NODE      = PATH=bin:${PATH} ${APP_BIN}/node
#
NODE_FILENAME = node-v${NODE_VERSION}-linux-x64

UNINSTALL_FILES += ${NODE_FILENAME} ${NODE_BIN} ${NPM_BIN} ${NPX_BIN} node_modules

APP_TARGETS += modules modules-update nvm
##

install: modules
update: modules-update

modules: package.json | ${NODE_BIN}         ## Deploy dependencies
	$I generate dependencies…
	$(NPM) install ${NODE_OPTIONS} || exit 1

modules-update: | ${NODE_BIN}              ## Force update dependencies
	$I updating all dependencies…
	$(NPM) update ${COMPOSER_OPTIONS} || exit 1

package.json: | ${NODE_BIN}
	$I create package config…
	$(NPM) init "${APP_PROJECT}" --yes

${NODE_BIN}: | ${BIN}
	$I installing Node…
	$(DOWNLOAD) ${BIN}/node.txz https://nodejs.org/dist/v${NODE_VERSION}/${NODE_FILENAME}.tar.xz || exit $$?
	tar xf ${BIN}/node.txz -C ${BIN} || exit $$?
	rm -rf ${BIN}/node.txz
	cd ${BIN} && ln -s ${NODE_FILENAME}/bin/* .

nvm:
	$I installing Node Version Manager…
	curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v${NVM_VERSION}/install.sh | bash
