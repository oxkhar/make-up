
## Params
PHPLOC_VERSION ?= 7.0.2
PHPLOC_OPTIONS ?= #

## Config
PHPLOC_BIN = ${BIN}/phploc
PHPLOC     = $(PHP_CLI) ${APP_BIN}/phploc

PHPLOC_REPORT = ${REPORT}/phploc
#
UNINSTALL_FILES += ${PHPLOC_BIN}

QA_TOOLS_BIN       += ${PHPLOC_BIN}
QA_TARGETS_ANALYSE += phploc
QA_TARGETS_REPORTS += phploc-report
##
phploc-report: ${REPORT} | ${PHPLOC_BIN}
	$I PHPLOC reports
	mkdir -p ${PHPLOC_REPORT}

	$(PHPLOC) \
		--log-csv=${PHPLOC_REPORT}/report.csv \
		--log-xml=${PHPLOC_REPORT}/report.xml \
		--log-json=${PHPLOC_REPORT}/report.json \
		${PHPLOC_OPTIONS} ${SOURCE}
	echo -e "Builded..."

phploc: | ${PHPLOC_BIN}                  ## PHPLOC
	$I PHPLOC

	$(PHPLOC) ${PHPLOC_OPTIONS} ${SOURCE}

${PHPLOC_BIN}: | ${BIN}
	$I Installing PHPLOC…

	$(DOWNLOAD) ${PHPLOC_BIN} \
		https://phar.phpunit.de/phploc-${PHPLOC_VERSION}.phar \
		|| exit $$?
	chmod +x ${PHPLOC_BIN}
