
## Params
KAHLAN_OPTIONS ?= # --no-colors --no-header
GENHTML        ?= $(DOCKER_RUN) genhtml

## Config
KAHLAN_BIN    = ${VENDOR_BIN}/kahlan
KAHLAN        = $(PHP_CLI) ${APP_BIN}/kahlan

KAHLAN_REPORT = ${REPORT}/spec
#
SPEC_TARGETS := spec-verbose spec-coverage spec-report

QA_TARGETS_REPORTS += spec-report
QA_REPORTS_TESTS    = phpmetrics spec-report
#
MK_DIRS += ${KAHLAN_REPORT}
UNINSTALL_FILES += infection.json infection.log ${KAHLAN_BIN}

APP_TARGETS += spec ${SPEC_TARGETS}
##
spec:  | ${KAHLAN_BIN}                              ## Run specs
	$I running $(SPEC_NAME:spec-%=% ) specs…
	$(KAHLAN) ${KAHLAN_OPTIONS} ${SPEC_ARGS}

	if [[ -e "${KAHLAN_REPORT}/coverage.info" ]]; then
	$(GENHTML) -o ${KAHLAN_REPORT}/coverage ${KAHLAN_REPORT}/coverage.info
	fi

spec-verbose:  SPEC_ARGS := --reporter=verbose    ## Run specs with description
spec-coverage: PHP_CLI := ${PHP_DEBUG}            ## Run specs with coverage reporting
spec-coverage: SPEC_ARGS := --coverage=3
spec-report:   PHP_CLI := ${PHP_DEBUG}            ## Run specs building report artifacts
spec-report:   SPEC_ARGS := --reporter=verbose \
                 --clover=${KAHLAN_REPORT}/coverage.xml \
                 --istanbul=${KAHLAN_REPORT}/coverage.json \
                 --lcov=${KAHLAN_REPORT}/coverage.info

${SPEC_TARGETS}: SPEC_NAME := $(filter spec-%, ${MAKECMDGOALS})
${SPEC_TARGETS}: ${REPORT} ${KAHLAN_REPORT} spec
